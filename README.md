TFT Library for pcDuino3
========================

The Ardiono library for the Arduino TFT LCD screen ported to pcDuino3 (TFT part, not sdcard part).
Ported by Iulian V., Apr. 2014, viulian@gmail.com

This is the first release, the pin connections are predefined in the library code instead of having them defined at runtime.

Connecting the TFT is almost identical to Arduino Leonardo pins with the following:
a) You can choose between the +5V or +3.3V pins offered by pcDuino3
b) SPI pins are the standard ones.

Video: https://www.youtube.com/watch?v=AdCxoOJOqog

*TFT screen connections to pcDuino3*

1. +5V    -> J9(5) +5V (or J9(4) +3.3V but not as bright)
2. MISO   -> J8(5) GPIO12 SPI0_MISO
3. SCK    -> J8(6) GPIO13 SPI0_CLK
4. MOSI   -> J8(4) GPIO11 SPI0_MOSI
5. LCD CS -> J11(8) GPIO7
6. SD CS  -> J8(1) GPIO8
7. D/C    -> J11(1) GPIO0
8. Reset  -> J11(2) GPIO1
9. BL     -> +3.3V
10. Ground -> J9(6) GND 

Original
========

An Arduino library for the Arduino TFT LCD screen.

This library enables an Arduino board to communicate with an Arduino TFT LCD screen. It simplifies the process for drawing shapes, lines, images, and text to the screen.
The Arduino TFT library extends the Adafruit GFX, and Adafruit ST7735 libraries that it is based on. The GFX library is responsible for the drawing routines, while the ST7735 library is specific to the screen on the Arduino GTFT. The Arduino specific additions were designed to work as similarly to the Processing API as possible.

Onboard the screen is a SD card slot, which can be used through the SD library.

The TFT library relies on the SPI library for communication with the screen and SD card, and needs to be included in all sketches.

https://github.com/adafruit/Adafruit-GFX-Library
https://github.com/adafruit/Adafruit-ST7735-Library
http://arduino.cc/en/Reference/SD
http://arduino.cc/en/Reference/SPI

http://arduino.cc/en/Reference/TFTLibrary
